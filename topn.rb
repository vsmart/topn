#!/usr/bin/env ruby

Block_size = 5
Out_Dir = './out/'

def topn(path, n)
  create_temp_dir
  sort_input_batches(path)
  sorted = find_n_smallest_numbers(n)

  puts "Results are: "
  puts sorted
end

def sort_input_batches(path)
  in_file = File.open(path)
  file_counter = 0

  while (s = handle_one_block(in_file)).any?
    write_to_out(s, file_counter)
    file_counter += 1
  end
end

def find_n_smallest_numbers(n)
  open_files_map = open_all_out_files
  ordered = []

  n.times do
    min = nil
    used_key = nil

    open_files_map.keys.each do |key|
      if !min || open_files_map[key] < min
        min = open_files_map[key]
        used_key = key
      end
    end

    ordered << min
    open_files_map = shift_to_next(open_files_map, used_key)
    break if open_files_map.empty?
  end

  close_files(open_files_map)
  ordered
end

def shift_to_next(open_files_map, used_key)
  next_number = line_to_int(used_key.gets)
  if next_number
    open_files_map[used_key] = next_number
  else
    open_files_map.delete(used_key)
  end
  open_files_map
end

def handle_one_block(file)
  n = 0
  sorted = []

  while (n < Block_size) && (line = file.gets)  do
    n += 1
    sorted << line_to_int(line)
  end

  sorted.sort
end

def create_temp_dir
  Dir.mkdir(Out_Dir)
end

def open_all_out_files
  open_files = {}
  Dir.foreach(Out_Dir) do |file|
    next if file == '.' or file == '..'
    open_file = File.open(Out_Dir + file, 'r')
    first_line = open_file.gets
    open_files[open_file] = line_to_int(first_line)
  end
  open_files
end

def close_files(open_files)
  open_files.keys.each do |file|
    file.close
  end
end

def write_to_out(sorted_vector, i)
  file = File.open("#{Out_Dir}part_#{i}", 'w')
  file.puts(sorted_vector)
  file.close
end

def line_to_int(line)
  line ? line.strip.to_i : nil
end

if ARGV.length == 2
  path = ARGV[0]
  n = ARGV[1].to_i
  topn(path, n)
else
  puts ">> Please give two arguments, file path and n. For example: `ruby topn.rb myfile 42`"
end
