# Topn

## What is this?

This is a small Ruby program that will sort large files. Given an input file and a number n, it will return the first n numbers from the file, sorted descendingly.

This program was written as part of the Gitlab coding challenge.

## How do I run it?

From a terminal, run the script giving the input filename and `n` as arguments.

For example:

```ruby
$ ruby topn.rb myfile 42
```

NB: As a side effect this program will create a directory '/out' with temporary files. It is safe to delete this after the program has completed.

## Requirements

You must have Ruby 2.0+ installed to run this as intended.

## Background

This program uses an _external merge sort_ algorith to sort large files.

### Further reading:

* https://en.wikipedia.org/wiki/External_sorting
* http://www.codeodor.com/index.cfm/2007/5/10/Sorting-really-BIG-files/1194
